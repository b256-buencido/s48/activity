// Dummy Database
let posts = [];
let count = 1;

// Create/Add Posts
document.querySelector("#form-add-post").addEventListener("submit", (event) => {

	// .preventDefault() this prevents the webpage from reloading/refreshing
	event.preventDefault();

	// acts like a schema/model of the document
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	// this is responsible to increment the ID value
	count++;

	console.log(posts);
	showPost(posts);
	alert("Successfully Added");
})

// Display/Show Posts
// Created a function to show all the data in our database
const showPost = (posts) => {

	// postEntries contain all individual posts
	let postEntries = "";

	// forEach will iterate the indices in our array
	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit Posts
const editPost = (postId) => {

	let title = document.querySelector(`#post-title-${postId}`).innerHTML;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector("#txt-edit-id").value = postId;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}

// Update Posts
document.querySelector("#form-edit-post").addEventListener("submit", (event) => {

	event.preventDefault();

	// to find  the specific post we want to edit
	for(let i=0; i<posts.length; i++) {

		// The values post[i].id is a NUMBER while document.querySelector("#txt-edit-id").value is a STRING
		// Therefore, it is necessary to convert the NUMBER to a toString
		if(posts[i].id.toString() === document.querySelector(`#txt-edit-id`).value) {

			posts[i].title = document.querySelector("#txt-edit-title").value
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPost(posts);
			alert("Successfully Updated!");

			break;
		}
	}
})

// Delete Post
const deletePost = (postId) => {

	document.querySelector(`#post-${postId}`).remove()
}